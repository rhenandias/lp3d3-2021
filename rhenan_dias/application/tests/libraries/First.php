<?php

class First extends TestCase {

  public function setUp(): void {
    $this->resetInstance();
  }

  // function testHello(){
  //   $this->CI->load->library('conta');
  //   $res = $this->CI->conta->soma(2, 3);
  //   $this->assertEquals(5, $res);
  // }
  
  // function testContaDeveRetornarRegistrosDoMesDezembro(){
  //   $this->CI->load->library('conta');
  //   $res = $this->CI->conta->lista('pagar', 1, 2021);
  //   $this->assertEquals(1, sizeof($res));
  // }

  // function xtestContaDeveInserirRegistroCorretamente(){
  //   $conta = [
  //     'parceiro' => 'Magalu De novo', 
  //     'descricao' => 'PC de novo', 
  //     'valor' => '999', 
  //     'mes' => 1, 
  //     'ano' => 2021, 
  //     'tipo' => 'pagar'
  //   ];

  //   $this->CI->load->library('conta');
  //   $this->CI->conta->cria($conta);
  //   $res = $this->CI->conta->lista('pagar', 1, 2021);
    
  //   $this->assertEquals(1, sizeof($res));
  //   $this->assertEquals(1, $res[0]['mes']);
  //   $this->assertEquals(2021, $res[0]['ano']);
  // }

  function testBuilder(){
    $this->CI->load->library('builder/ContaDataBuilder', null, 'builder');
    $this->CI->builder->clear();
    $this->CI->builder->start();

    $this->CI->load->library('conta');
    $res = $this->CI->conta->lista('pagar', 1, 2021);

    $this->assertEquals('Magalu', $res[0]['parceiro']);
    $this->assertEquals(3, sizeof($res));
    $this->assertEquals(1, $res[0]['mes']);
    $this->assertEquals(2021, $res[0]['ano']);
  }

  function testContaDeveInformarTotalDeContasAPagarEAReceber(){
    $this->CI->load->library('builder/ContaDataBuilder', null, 'builder');
    $this->CI->builder->clear();
    $this->CI->builder->start();

    $this->CI->load->library('conta');
    $res = $this->CI->conta->total('pagar', 1, 2021);
    $this->assertEquals(300, $res);
  }

  function testContaDeveCalcularSaldoMensal(){
    $this->CI->load->library('builder/ContaDataBuilder', null, 'builder');
    $this->CI->builder->clear();
    $this->CI->builder->start();

    $this->CI->load->library('conta');
    $res = $this->CI->conta->saldo(1, 2021);
    $this->assertEquals(200, $res);
  }

}