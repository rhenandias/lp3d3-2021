<div class="container">

  <?php echo form_error('parceiro', '<div class="alert alert-danger">', '</div>'); ?>
  <?php echo form_error('descricao', '<div class="alert alert-danger">', '</div>'); ?>
  <?php echo form_error('valor', '<div class="alert alert-danger">', '</div>'); ?>
  <?php echo form_error('ano', '<div class="alert alert-danger">', '</div>'); ?>
  <?php echo form_error('mes', '<div class="alert alert-danger">', '</div>'); ?>

  <!-- Buttons trigger collapse -->
  <form method="POST">
  <div class="row mt-5">
    <div class="col-md-2">
      <a class="btn btn-primary my-4" data-mdb-toggle="collapse" href="#collapseForm" role="button" aria-expanded="false" aria-controls="collapseExample">
        Nova Conta
      </a>
    </div>
    <div class="col-md-2 offset-md-7 mt-3">
      <input type="month" id="month" name="month" value="<?= set_value('month') ?>">
    </div>
  </div>



  <div class="div row">
    <div class="collapse mt-3" id="collapseForm">
      <div class="div col-md-6 mx-auto border p-4">
          <div class="form-outline mb-4">
            <input type="text" value="<?= set_value('parceiro') ?>" class="form-control" name="parceiro" id="parceiro" />
            <label class="form-label">Devedor / Credor</label>
          </div>
          <div class="form-outline my-x">
            <input type="text" value="<?= set_value('descricao') ?>" class="form-control" name="descricao" id="descricao"/>
            <label class="form-label">Descrição</label>
          </div>
          <div class="form-outline my-4">
            <input type="number" value="<?= set_value('valor') ?>" class="form-control" name="valor" id="valor"/>
            <label class="form-label">Valor</label>
          </div>

          <!-- <div class="row mb-4 my-4">
            <div class="col">
              <div class="form-outline">
                <input type="number" value="<?= set_value('mes') ?>" class="form-control" name="mes" id="mes"/>
                <label class="form-label">Mês</label>
              </div>
            </div>
            <div class="col">
              <div class="form-outline">
                <input type="number" value="<?= set_value('ano') ?>" class="form-control" name="ano" id="ano"/>
                <label class="form-label">Ano</label>
              </div>
            </div>
          </div> -->

          <input type="hidden" name="id" id="conta_id" />
          <input type="hidden" name="tipo" value="<?= $tipo ?>" />

          <div class="d-grid gap-2">
            <button class="btn btn-primary" type="submit">Enviar</button>
          </div>
        </form>
      </div>
    </div>

    <div class="row">
      <div class="col">
        <?= $lista ?>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div
  class="modal fade"
  id="exampleModal"
  tabindex="-1"
  aria-labelledby="exampleModalLabel"
  aria-hidden="true"
>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Remoção de Contas</h5>
        <button
          type="button"
          class="btn-close"
          data-mdb-dismiss="modal"
          aria-label="Close"
        ></button>
      </div>
      <div class="modal-body">
        Deseja realmente apagar esta conta? Essa ação é permanente.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-mdb-dismiss="modal">
          Cancelar
        </button>
        <button type="button"  id="confirmBtn" class="btn btn-primary">Remover</button>
      </div>
    </div>
  </div>
</div>

<script>
  var row_id = 0;

  $(document).ready(function() {
    $('.delete_btn').click(openModal);
    $("#confirmBtn").click(deleteRow);
    $('.edit_btn').click(exibeForm);
    $('.pay_btn').click(liquidaConta);
    $('#month').change(loadMonth);
  });

  function loadMonth(){
    var data = this.value.split('-');
    var ano = data[0];
    var mes = data[1];

    var v = window.location.href.split("/");
    var url = v.slice(0, 7).join("/");
    url = url + "/" + mes + "/" + ano;
    window.location.href = url;
  }

  function exibeForm(){
    row_id = this.id;
    
    var td = $('#'+row_id).parent().parent().parent().children();
    
    $("#parceiro").val($(td[0]).text());
    $("#descricao").val($(td[1]).text());
    $("#valor").val($(td[2]).text());
    $("#mes").val($(td[3]).text());
    $("#ano").val($(td[4]).text());
    $("#conta_id").val(row_id);

    $("#collapseForm").collapse('show');
  }

  function openModal(){
    row_id = this.id;
    $("#exampleModal").modal('show');
  }
  
  function deleteRow(){
    var id = row_id;
    $.post(api('contas', 'delete_conta'), {id: id}, (d, s, x) => {
      $('#'+row_id).parent().parent().parent().remove();
      $("#exampleModal").modal('hide');
    });
  }

  function liquidaConta(){
    var id = this.id;
    $.post(api('contas', 'status_conta'), {id: id}, (d, s, x) => {
     $("#" + id).toggleClass('text-muted text-success');
    });
  }

</script>