<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>LP2D3 - 2021</title>
    <link rel="icon" href="<?= base_url() ?>/assets/mdb/img/mdb-favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.2/css/all.css" />
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap"
    />
    <link rel="stylesheet" href="<?=base_url() ?>/assets/mdb/css/mdb.min.css" />

    <!-- <script type="text/javascript" src="<?= base_url() ?>/assets/mdb/js/jquery.js"></script> -->
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
  </head>
  <body>
