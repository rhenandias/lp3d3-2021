<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/util/CI_Object.php';

class ContaDataBuilder extends CI_Object {

  private $contas = [
    [
      'parceiro' => 'Magalu', 
      'descricao' => 'Computador', 
      'valor' => '100', 
      'mes' => 1, 
      'ano' => 2021, 
      'tipo' => 'pagar'
    ],
    [
      'parceiro' => 'NET', 
      'descricao' => 'Internet', 
      'valor' => '100', 
      'mes' => 1, 
      'ano' => 2021, 
      'tipo' => 'pagar'
    ],
    [
      'parceiro' => 'SAAE', 
      'descricao' => 'ÁGua', 
      'valor' => '100', 
      'mes' => 1, 
      'ano' => 2021, 
      'tipo' => 'pagar'
    ],
    [
      'parceiro' => 'Empresa', 
      'descricao' => 'Estágio', 
      'valor' => '500', 
      'mes' => 1, 
      'ano' => 2021, 
      'tipo' => 'receber'
    ]
  ];

  public function start(){
    $this->load->library('conta');

    foreach ($this->contas as $conta){
      $this->conta->cria($conta);
    }
  }

  public function clear(){
    $this->db->truncate('conta');
  }

}