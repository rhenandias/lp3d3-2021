<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/util/CI_Object.php';

class Conta extends CI_Object {

  public function cria($data){
    // O professor colocou esse código aqui na aula
    // No projeto local funcionou normalmente
    // Essa linha estava no arquivo ContasModel.php
    unset($data['month']);

    $this->db->insert('conta', $data);
    return $this->db->insert_id();
  }

  public function lista($tipo, $mes, $ano){
    $data = ['mes' => $mes, 'ano' => $ano];

    if(strcmp($tipo, 'mista') != 0){
      $data['tipo'] = $tipo;
    }

    $res = $this->db->get_where('conta', $data);
    return $res->result_array();
  }

  public function delete($data){
    $this->db->delete('conta', $data);
  }

  public function edita($data){
    $this->db->update('conta', $data, 'id = '.$data['id']);;
  }

  public function status($data){
    $sql = "update conta set liquidada = liquidada + 1 where id = ".$data['id'];
    $this->db->query($sql);
  }

  public function total($tipo, $mes, $ano){
    $conds = ['tipo'=> $tipo, 'mes' => $mes, 'ano' => $ano];
    $this->db->select_sum('valor');
    $query = $this->db->get_where('conta', $conds);
    return $query->row()->valor;
  }

  public function saldo($mes, $ano){
    $rec = $this->total('pagar', $mes, $ano);
    $pag = $this->total('receber', $mes, $ano);
    return $pag - $rec;
  }
}